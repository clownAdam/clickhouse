package com.jds.clickhouse.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jds.clickhouse.model.entity.mysql.AffiliationRelationClickhouse;
import com.jds.clickhouse.model.entity.mysql.AffiliationRelationMysql;
import com.jds.clickhouse.model.mapper.clickhouse.AffiliationRelationClickhouseMapper;
import com.jds.clickhouse.model.mapper.mysql.AffiliationRelationMysqlMapper;
import com.jds.clickhouse.web.ResultJson.ResultJson;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: zhixiang.peng
 * @date: 2021/3/15
 * @description:
 */
@RestController
@RequiredArgsConstructor
public class TestController {


    private final AffiliationRelationMysqlMapper mysqlMapper;
    private final AffiliationRelationClickhouseMapper clickhouseMapper;

    @GetMapping("/getMysqlCount")
    public ResultJson<Object> getMysqlCount() {
        long start = System.currentTimeMillis();
        Integer count = mysqlMapper.selectCount(new LambdaQueryWrapper<AffiliationRelationMysql>());
        long end = System.currentTimeMillis();
        return ResultJson.success("mysql执行count查询耗时（ms）:" + (end - start) , "数据条数:"+count);
    }
    @GetMapping("/getClickhouseCount")
    public ResultJson<Object> getClickhouseCount() {
        long start = System.currentTimeMillis();
        Integer count = clickhouseMapper.selectCount(new LambdaQueryWrapper<AffiliationRelationClickhouse>());
        long end = System.currentTimeMillis();
        return ResultJson.success("clickhouse执行count查询耗时（ms）:" + (end - start)  , "数据条数:"+count);
    }
}
